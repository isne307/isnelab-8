#include <iostream>
#include <list>
#include <string>
#include "Graph.h"
using namespace std;
int minDistance(int dist[], bool sptSet[], int N);
int main(){
	list<int> ArrayList;
	Graph earn;
	int node;

    // dimensions
    cout<<"Enter a number of nodes: ";
    cin>>node;

    // dynamic allocation
    int row = node, col = node;
    int** ary = new int*[row];
    for(int i = 0; i < row; ++i){ary[i] = new int[col];}

    // fill
    cout<<"Enter a number of the path between each node: ";
    for(int i = 0; i < row; ++i){for(int j = 0; j < col; ++j)cin>>ary[i][j];}

    // print
    cout<<"\nAdjacent Matrix of this graph is: "<<endl;
    for(int i = 0; i < row; ++i)
        for(int j = 0; j < col; ++j){
            cout<<ary[i][j]<<" ";
            if(j == col-1){cout<<endl;}
        }

    // Send all of the value into the list
	for(int i = 0; i < row; i++){
		for(int j = 0; j < col; j++){
			ArrayList.push_back(ary[i][j]);
		}
	}

    // Call function in Graph.h
    // Send array list with its size
	earn.pushInList(node,ArrayList);

	cout<<"\nAdjacent List of this graph is: "<<endl;
	earn.showList(node);
	cout<<endl;
	if(earn.Multig(node))      {cout<<endl<<"This graph is MultiGraph";}
	if(earn.Pseudog(node))     {cout<<endl<<"This graph is PseudoGraph";}
    if(earn.Directg(node))     {cout<<endl<<"This graph is DirectGraph";}
	if(earn.Weightg(node))     {cout<<endl<<"This graph is WeightGraph";}
	if(earn.Completeg(node))   {cout<<endl<<"This graph is CompleteGraph";}
	cout<<endl;
    earn.Dijkstra(node, ary);
    earn.Prim(node, ary);
    earn.Kruskal(node, ary);

	// clean up
	for(int i = 0; i < row; ++i) {delete [] ary[i];}
    delete [] ary;

	return 0;
}
